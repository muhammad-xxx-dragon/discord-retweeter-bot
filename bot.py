import discord

from decouple import config
from discord.ext import commands
from tweet_handler import TweetHandler


TOKEN = config('DISCORD_TOKEN')

client = commands.Bot(command_prefix='!', help_command=None)


@client.event
async def on_ready():
    print("Bot is ready")


@client.command(aliases=['Hi', 'hi', 'Hello'])
async def hello(ctx):
    await ctx.send(f"Wassup!")


@client.command(aliases=['t', 'twt'])
async def tweet(ctx, username=" "):
    twt = TweetHandler()
    response = twt.get_latest_tweet(username)
    await ctx.send(response)


@client.command()
async def help(ctx):
    await ctx.send(
        f"```Get the latest tweet from specific person!\n\n"
        + f"!Hi: to greet (or maybe check my current status)\n"
        + f"!help: get the list of commands\n"
        + f"!t <twitter_username>: get the latest tweet from particular user\n\n"
        + f"To contribute to this retweeter bot go to "
        + f"https://gitlab.com/muhammad-xxx-dragon/discord-retweeter-bot```"
    )

client.run(TOKEN)
