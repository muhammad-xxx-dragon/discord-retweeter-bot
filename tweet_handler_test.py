import unittest

from tweet_handler import TweetHandler, get_tweet_link


class TestStringMethods(unittest.TestCase):
    def test_get_tweet_link(self):
        link = get_tweet_link('babibu', 'ladidu')
        self.assertEqual(link, 'https://twitter.com/babibu/status/ladidu')

    def test_return_right_response_with_0_tweet_id(self):
        tweet_handler = TweetHandler()
        response = tweet_handler.get_latest_tweet("@rog_schn")
        self.assertEqual(response, "No tweet from @rog_schn found")

    def test_handles_TweepError_correctly(self):
        tweet_handler = TweetHandler()
        response = tweet_handler.get_latest_tweet("@rog.schn")
        self.assertEqual(response,
                         "I can't process that, make sure you typed the correct user id")


if __name__ == '__main__':
    unittest.main()
