from decouple import config
import tweepy
import logging

CONSUMER_KEY = config('TWT_KEY')
CONSUMER_SECRET_KEY = config('TWT_SECRET_KEY')


class TweetHandler:
    # Class to handle all things Twitter API that the bot will use

    def __init__(self):
        auth = tweepy.AppAuthHandler(CONSUMER_KEY, CONSUMER_SECRET_KEY)
        self.api = tweepy.API(auth)

    def get_latest_tweet(self, user_id):
        try:
            # Trying to get the 'count' latest tweet from 'user_id'
            for tweet in self.api.user_timeline(user_id, count=1):
                return get_tweet_link(user_id, tweet.id_str)
            else:
                return ('No tweet from '+user_id+' found')
        except tweepy.TweepError as e:
            # There might be a better way to handle Tweepy error because
            # This includes various type of error from twitter API
            logging.error('Tweepy Error')
            return ("I can't process that, make sure you typed the correct user id")
        except tweepy.RateLimitError as e:
            return ("I can't respond that fast, wait a bit")


def get_tweet_link(user_id, tweet_id):
    return ('https://twitter.com/'+user_id+'/status/'+tweet_id)
