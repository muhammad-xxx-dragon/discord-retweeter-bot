# Project Akhir PMPL - Muhammad XXX Dragon

## Discord bot: retweeter bot

Bot Discord yang bisa digunakan untuk mengecek tweet terbaru dari sebuah akun twitter.

## Instalasi

Untuk menjalankan bot, ikuti langkah berikut.

#### Membuat akun bot

- Pastikan sudah memiliki akun discord.

- Login discord.

- Buat aplikasi untuk Discord di [Developer Portal Discord](https://discord.com/developers/applications)

- Pada menubar sebelah kiri, klik `bot`. Klik `Add bot` kemudian `Yes do it!`

- Kamu bisa memilih untuk mem-private bot kamu.

- Jangan lupa klik `Save change`.

- Copy token yang kamu dapat ke suatu tempat karena akan kita gunakan kembali untuk menjalankan botnya.

#### membuat server environment test

- Buat server untuk mengetes bot kita.

- Pada sidebar sebelah kiri, klik `OAuth2`.

- Atur scope menjadi bot dan buat bot menjadi administrator.

- Buka URL yang diberikan oleh server.

- Pada halaman URL tersebut, pilih server yang telah dibuat dan klik `Continue`.

#### menjalankan bot di lokal

- Install library `discord.py` dengan menjalankan command berikut pada cmd:

```shell
pip install discord.py
```

- Buat file `.env`, masukan kode berikut di dalamnya:

```shell
DISCORD_TOKEN = {masukkan kode bot kamu}
```

- Jalankan command berikut untuk menjalankan bot:

```shell
python bot.py
```

## Command

<details>
<summary markdown="span">!Hi</summary>
![Output command !Hi](https://i.imgur.com/DEHFIKH.png)
</details>
<details>
<summary markdown="span">!help</summary>
![Output command !help](https://i.imgur.com/37Z5PRo.png)
</details>
<details>
<summary markdown="span">!t</summary>
![Output command !t](https://i.imgur.com/HrdGGhN.png)
</details>

## Sonarcloud Link
https://sonarcloud.io/dashboard?id=muhammad-xxx-dragon_discord-retweeter-bot

## Progress 1

- Inisiasi Project.
- Menambahkan command dengan keluaran `Hello World!`.
- Membuat handler untuk mendapatkan link tweet terakhir dari suatu user yang diinginkan (belum berhasil di tes secara otomatis)
- Mempelajari API twitter dan library untuk testing discord bot.
- Mencoba integrasi dengan sonarqube (belum selesai).

## Progress 2
- Integrasi discord bot dengan twitter api handler.  
- Menambahkan command `!help` untuk menampilkan.  
- Integrasi sonarqube (dengan sonarcloud), sonarcloud belum bisa memberikan feedback yang bermakna karena proses testing pada remote repository belum dilakukan. 

## Progress 3
- Automated testing (CI/CD)  
- Automated linting (CI/CD)
- Automated deployment (CI/CD)
